﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class DSWebstoreWidgetService : WebStoreWidgetService
    {
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _widgetProductMongoRepository;

        public DSWebstoreWidgetService() : base()
        {

            _widgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            //_productMongoService = new ProductMongoService();
            ////_linkMongoRepository = new MongoRepository<WidgetTitleEntity>();
            ////_widgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            ////_categoryMongoService = new CategoryMongoService();
            //_productMongoRepository = new MongoRepository<ProductEntity>();
            //_publishProductService = new PublishProductService();
            //// _widgetTextEntity = new MongoRepository<TextWidgetEntity>();
            //_awctAttributeSwatchHelper = new AWCTAttributeSwatchHelper();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();

        }


        public override WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
            webStoreLinkProductListModel = base.GetLinkProductList(parameter, expands);
            webStoreLinkProductListModel = GetSwatchImagesForLinkProductsOnPDP(webStoreLinkProductListModel, parameter.PortalId);
            return webStoreLinkProductListModel;
        }

        private WebStoreLinkProductListModel GetSwatchImagesForLinkProductsOnPDP(WebStoreLinkProductListModel webStoreLinkProductListModel, int portalId)
        {
            #region Commented code
            //try
            //{
            //    ImageHelper image = new ImageHelper(portalId);
            //    string ProductImageName = "";

            //    string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //    string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
            //    string _imagePath = "";
            //    string _swatchImagePath = "";


            //    int index = 0;

            //    //ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //    index = ImageSmallPath.LastIndexOf('/');
            //    if (index != -1)
            //        _imagePath = ImageSmallPath.Substring(0, index) + "/";

            //    //OriginalImagepath = http://localhost:44762/Data/Media/
            //    index = OriginalImagepath.LastIndexOf('/');
            //    if (index != -1)
            //    {
            //        _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //        _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //    }


            //    int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            //    for (int i = 0; i < _linkproductcount; i++)
            //    {
            //        List<ProductAlterNateImageModel> simage = GetSwatchImages(String.Join(",", webStoreLinkProductListModel.LinkProductList[i].PublishProduct.Select(x => x.PublishProductId).ToList()), _imagePath, _swatchImagePath, "Home");
            //        foreach (PublishProductModel prd1 in webStoreLinkProductListModel.LinkProductList[i].PublishProduct)
            //        {
            //            if (prd1.AlternateImages == null)
            //                prd1.AlternateImages = new List<ProductAlterNateImageModel>();
            //            prd1.AlternateImages = simage.Where(x => x.FileName == prd1.PublishProductId.ToString()).ToList<ProductAlterNateImageModel>();
            //        }
            //    }
            //}
            //catch(Exception ex)
            //{
            //    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            //}
            #endregion
            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            for (int i = 0; i < _linkproductcount; i++)
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "drewcolor");
            }
            return webStoreLinkProductListModel;

        }
        public override WebStoreWidgetProductListModel GetProducts(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            List<WidgetProductEntity> productListWidgetEntity = _widgetProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollectionForProducts(parameter).ToFilterMongoCollection()));

            IMongoQuery query = Query.And(
            Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, parameter.PublishCatalogId),
            Query<CategoryEntity>.EQ(pr => pr.IsActive, true),
            Query<CategoryEntity>.EQ(pr => pr.LocaleId, parameter.LocaleId),
            Query<CategoryEntity>.EQ(pr => pr.VersionId, GetCatalogVersionId(parameter.PublishCatalogId, parameter.LocaleId))
            );

            List<CategoryEntity> categoryListWidgetEntity = _categoryMongoRepository.Table.MongoCollection.Find(query).ToList();
            foreach (CategoryEntity item in categoryListWidgetEntity)
            {
                if ((item.ActivationDate == null || item.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (item.ExpirationDate == null || item.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                    parameter.CategoryIds += item.ZnodeCategoryId.ToString() + ",";
            }

            expands = new NameValueCollection();
            PublishProductListModel model = GetPublishProducts(expands, ProductListFilters(parameter, string.Join(",", productListWidgetEntity?.OrderBy(x => x.DisplayOrder)?.Select(x => x.SKU))), null, null);
            if (model.PublishProducts != null && model.PublishProducts.Count > 0)
            {
                model.PublishProducts = AssignSwatchImages(model?.PublishProducts, parameter.PortalId);
            }
            WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts);

            listModel.DisplayName = parameter.DisplayName;
            return listModel;
        }
        private List<PublishProductModel> AssignSwatchImages(List<PublishProductModel> publishProducts, int portalId)
        {
            #region Commented Code
            //ImageHelper image = new ImageHelper(portalId);
            //string ProductImageName = "";

            //string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
            //string _imagePath = "";
            //string _swatchImagePath = "";


            //int index = 0;

            ////ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //index = ImageSmallPath.LastIndexOf('/');
            //if (index != -1)
            //    _imagePath = ImageSmallPath.Substring(0, index) + "/";

            ////OriginalImagepath = http://localhost:44762/Data/Media/
            //index = OriginalImagepath.LastIndexOf('/');
            //if (index != -1)
            //{
            //    _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //    _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //}

            //List<ProductAlterNateImageModel> simage = GetSwatchImages(String.Join(",", publishProducts.Select(x => x.PublishProductId).ToList()), _imagePath, _swatchImagePath, "Home");
            //foreach (PublishProductModel prd in publishProducts)
            //{
            //    if (prd.AlternateImages == null)
            //        prd.AlternateImages = new List<ProductAlterNateImageModel>();
            //    prd.AlternateImages = simage.Where(x => x.FileName == prd.PublishProductId.ToString()).ToList<ProductAlterNateImageModel>();
            //}
            #endregion
            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "drewcolor");

            return publishProducts;
        }
        private WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();

            foreach (PublishProductModel data in model)
            {
                WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                webStoreWidgetProductModel.WebStoreProductModel = MapData(data);
                webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);
            }
            return webStoreWidgetProductListModel;
        }
        private FilterCollection ProductListFilters(WebStoreWidgetParameterModel parameter, string SKUs)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(WebStoreEnum.SKU.ToString(), FilterOperators.In, SKUs));
            filter.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            if (!string.IsNullOrEmpty(parameter.CategoryIds))
                filter.Add(new FilterTuple(FilterKeys.CategoryIds, FilterOperators.In, parameter.CategoryIds?.TrimEnd(',')));
            if (parameter.ProductProfileId > 0)
                filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
            return filter;
        }
        //Map PublishProductModel to WebStoreProductModel.
        private WebStoreProductModel MapData(PublishProductModel model)
        {
            return new WebStoreProductModel
            {
                Attributes = model.Attributes,
                LocaleId = model.LocaleId,
                CatalogId = model.PublishedCatalogId,
                PublishProductId = model.PublishProductId,
                Name = model.Name,
                RetailPrice = model.RetailPrice,
                SalesPrice = model.SalesPrice,
                PromotionalPrice = model.PromotionalPrice,
                SKU = model.SKU,
                ProductReviews = model.ProductReviews,
                CurrencyCode = model.CurrencyCode,
                ImageMediumPath = model.ImageMediumPath,
                ImageSmallPath = model.ImageSmallPath,
                ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                ImageThumbNailPath = model.ImageThumbNailPath,
                ImageLargePath = model.ImageLargePath,
                SEODescription = model.SEODescription,
                SEOTitle = model.SEOTitle,
                Rating = model.Rating,
                TotalReviews = model.TotalReviews,
                SEOUrl = model.SEOUrl,
                SEOKeywords = model.SEOKeywords,
                GroupProductPriceMessage = model.GroupProductPriceMessage,
                Promotions = model.Promotions,
                AlternateImages = model.AlternateImages

            };
        }
        private FilterCollection CreateFilterCollectionForProducts(WebStoreWidgetParameterModel parameter)
      => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Like, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
        };
    }
}
