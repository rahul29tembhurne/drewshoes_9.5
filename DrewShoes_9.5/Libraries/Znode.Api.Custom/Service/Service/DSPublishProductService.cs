﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Utilities = Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class DSPublishProductService : PublishProductService
    {
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;

        public DSPublishProductService() : base()
        {
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _ProductMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());


        }
        public override PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            string productType;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);
            productType = filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.ProductType, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
            ZnodeLogging.LogMessage("productType:", string.Empty, TraceLevel.Verbose, new object[] { productType });
            if (!string.IsNullOrEmpty(productType))
            {
                filters.RemoveAll(x => x.Item1 == FilterKeys.ProductType);
                filters.Add(FilterKeys.AttributeValueForProductType, FilterOperators.Contains, productType);
            }

            if (!string.IsNullOrEmpty(productType))
            {
                filters.RemoveAll(x => x.Item1 == FilterKeys.ProductType);
                filters.Add(FilterKeys.AttributeValueForProductType, FilterOperators.Contains, productType);
            }

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

            int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new object[] { versionId = versionId, catalogId = catalogId });
            //get catalog current version id by catalog id.
            if (catalogId > 0)
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(versionId));
            else
            {
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, filters.Exists(x => x.Item1 == Utilities.FilterKeys.RevisionType) ? GetCatalogAllVersionIds(localeId) : GetCatalogAllVersionIds());

                //Get comma separated ids of the catalogs which are associated to the store(s).
                string activeCatalogIds = string.Join(",", (from portalCatalog in _portalCatalogRepository.Table
                                                            join publishCatalog in _publishCatalogRepository.Table on portalCatalog.PublishCatalogId equals publishCatalog.PublishCatalogId
                                                            select portalCatalog.PublishCatalogId).Distinct().ToArray());
                ZnodeLogging.LogMessage("activeCatalogIds:", string.Empty, TraceLevel.Verbose, new object[] { activeCatalogIds });
                filters.Add(FilterKeys.CatalogId, FilterOperators.In, activeCatalogIds);
            }
            if (filters.Exists(x => x.Item1 == FilterKeys.RevisionType))
                filters.RemoveAll(x => x.Item1 == FilterKeys.RevisionType);
            if (userId > 0 && EnableProfileBasedSearch(portalId))
            {
                int? userProfileId = GetUserProfileId(userId);
                ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { userProfileId = userProfileId, userId = userId });
                if (!Equals(userProfileId, null))
                    filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
            }
            //Get filter value
            string filterValue = filters.FirstOrDefault(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In)?.FilterValue;
            ZnodeLogging.LogMessage("filterValue:", string.Empty, TraceLevel.Verbose, new object[] { filterValue });
            if (!string.IsNullOrEmpty(filterValue))
            {
                //Remove Payment Setting Filters with IN operator from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In);

                //Add Payment Setting Filters
                filters.Add(FilterKeys.AttributeValuesForPromotion, FilterOperators.In, filterValue.Replace('_', ','));
            }

            if (filters.Any(w => w.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower()))
            {
                //Remove ZnodeCategoryId Filters from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower());
                filters.Add(FilterKeys.ZnodeCategoryId, FilterOperators.In, GetActiveCategoryIds(catalogId));
            }

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            //Check if products are taken for some specific category.
            SetProductIndexFilter(filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to publishProducts list :", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
            //get publish products from mongo
            List<ProductEntity> publishProducts = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProducts list count:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, publishProducts?.Count());
            PublishProductListModel publishProductListModel = new PublishProductListModel() { PublishProducts = publishProducts.ToModel<PublishProductModel>().ToList() };

            GetExpands(portalId, localeId, expands, publishProductListModel, versionId);

            //Map pagination parameters
            publishProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, publishProductId);

            bool isChildPersonalizableAttribute = false;
            List<PublishAttributeModel> parentPersonalizableAttributes = null;
            int portalId, localeId;
            int? catalogVersionId;
            PublishProductModel publishProduct = null;
            List<ProductEntity> products;
            string parentProductImageName = null;

            //Get publish product from mongo
            products = GetPublishProductFromMongo(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            List<int> associatedCategoryIds = new List<int>();

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
                IEnumerable<int> categoryIds = products.Select(x => x.ZnodeCategoryIds);

                List<IMongoQuery> query = new List<IMongoQuery>();
                query.Add(Query<CategoryEntity>.In(d => d.ZnodeCategoryId, categoryIds));
                query.Add(Query<CategoryEntity>.EQ(d => d.IsActive, true));
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

                List<CategoryEntity> categoryEntities = _categoryMongoRepository.GetEntityList(Query.And(query), true);

                associatedCategoryIds.AddRange(categoryEntities.Select(x => x.ZnodeCategoryId));

                publishProduct = products.FirstOrDefault(x => associatedCategoryIds.Contains(x.ZnodeCategoryIds)).ToModel<PublishProductModel>();

                parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                ConfigurableProductEntity configEntiy = GetConfigurableProductEntity(publishProductId, catalogVersionId);

                if (HelperUtility.IsNotNull(configEntiy))
                {
                    //Selecting child SKU
                    IEnumerable<string> childSKU = publishProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();
                    //Get Associated product by child SKUs
                    List<ProductEntity> associatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, childSKU);

                    //Nivi Code start for Selecting parent SKU
                    IEnumerable<string> parentSKU = publishProduct.Attributes.Where(x => x.AttributeCode == "SKU").Select(y => y.AttributeValues);

                    //Get Associated product by parent SKUs
                    List<ProductEntity> parentassociatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, parentSKU);
                    List<AttributeEntity> ConfigData = new List<AttributeEntity>();

                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ShortDescription").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "fallundercategory").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ProductSpecification").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "Highlights").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourl360").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourllifestyle").ToList());

                    /*removing existing data from list*/
                    publishProduct.Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

                    parentassociatedProducts.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

                    /*inserting Config product data to simple products*/
                    associatedProducts.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));
                    //Nivi Code end for Selecting parent SKU

                    parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();

                    publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.ConfigurableAttributeCodes, catalogVersionId.GetValueOrDefault());
                }
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault(), null, GetProfileId());

                isChildPersonalizableAttribute = publishProduct.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                GetProductImagePath(portalId, publishProduct, true, parentProductImageName);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);

                publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;
            }
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
        }

        private List<ProductEntity> GetPublishProductFromMongo(int publishProductId, FilterCollection filters, out int portalId, out int localeId, out int? catalogVersionId, PublishProductModel publishProduct, out List<ProductEntity> products)
        {
            //Get parameter values from filters.
            int catalogId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            publishProduct = null;
            //Get publish product from mongo
            products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);

            return products;
        }
        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return publishProduct;
        }
        
        public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            PublishProductModel product = null;
            //new implementaion
            int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ProductEntity>.EQ(d => d.ZnodeProductId, productAttributes.ParentProductId));
            query.Add(Query<CategoryEntity>.EQ(d => d.IsActive, true));
            query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, versionId));

            ProductEntity parentProduct = _ProductMongoRepository.GetEntity(Query.And(query));

            //Selecting child SKU
            IEnumerable<string> childSKU = parentProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();
            //Creating new query
            List<ProductEntity> productList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);

            foreach (var item in productAttributes.SelectedAttributes)
            {

                productList = (from productentity in productList
                               from attribute in productentity.Attributes
                               where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                               select productentity).ToList();
            }

            ProductEntity productEntity = productList.FirstOrDefault();


            //List<string> list = new List<string>(childSKU);

            //Original Code
            //if (HelperUtility.IsNull(productEntity))
            //{
            //    //Creating new query
            //    List<ProductEntity> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
            //    product = newproductList?.FirstOrDefault().ToModel<PublishProductModel>();
            //    product.IsDefaultConfigurableProduct = true;
            //}
            //else
            //    product = productEntity?.ToModel<PublishProductModel>();

            /*Nivi Code end For New Attributes*/

            IEnumerable<string> parentSKU = parentProduct.Attributes.Where(x => x.AttributeCode == "SKU").Select(y => y.AttributeValues);


            //Get Associated product by parent SKUs
            List<ProductEntity> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, parentSKU);


            List<AttributeEntity> ConfigData = new List<AttributeEntity>();

            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ShortDescription").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "fallundercategory").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ProductSpecification").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "Highlights").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourl360").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourllifestyle").ToList());
            /*removing existing data from list*/
            parentProduct.Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));
            newproductList.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

            /*inserting Config product data to simple products*/
            //newproductList.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));


            if (HelperUtility.IsNull(productEntity))
            {
                ////Creating new query
                List<ProductEntity> ChildnewproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
                ChildnewproductList.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));
                ChildnewproductList.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));
                product = ChildnewproductList?.FirstOrDefault().ToModel<PublishProductModel>();
                product.IsDefaultConfigurableProduct = true;
            }
            else
            {
                //product = productEntity?.ToModel<PublishProductModel>();
                string productEntityChildSKU = productEntity.SKU;
                newproductList.FirstOrDefault().Attributes.RemoveAll(x => x.AttributeCode == "SKU");
                newproductList.ForEach(x => x.SKU = productEntityChildSKU);
                newproductList.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));
                product = newproductList?.FirstOrDefault().ToModel<PublishProductModel>();


                //product.IsDefaultConfigurableProduct = false;

            }

            /*Nivi Code end For New Attributes*/


            if (HelperUtility.IsNotNull(product))
            {
                bool isChildPersonalizableAttribute = product.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                var parentPersonalizableAttributes = parentProduct.Attributes?.Where(x => x.IsPersonalizable);
                product.AssociatedGroupProducts = productList.ToModel<WebStoreGroupProductModel>().ToList();

                product.ConfigurableProductId = productAttributes.ParentProductId;
                product.IsConfigurableProduct = true;

                product.ProductType = ZnodeConstant.ConfigurableProduct;
                product.ConfigurableProductSKU = product.SKU;
                product.SKU = productAttributes.ParentProductSKU;

                publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());

                GetProductImagePath(productAttributes.PortalId, product);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(productAttributes.PortalId, product);

                if (!isChildPersonalizableAttribute && parentPersonalizableAttributes?.Count() > 0)
                    product.Attributes.AddRange(parentPersonalizableAttributes.ToModel<PublishAttributeModel>().ToList());
            }
            return product;
        }
    }
}
