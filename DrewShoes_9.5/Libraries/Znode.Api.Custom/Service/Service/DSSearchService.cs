﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class DSSearchService : SearchService
    {
        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            if (model.PageSize == 16)
                model.PageSize = 40;

            int portalId; int catalogId;
            int localeId;
            GetParametersValueForFilters(filters, out portalId, out catalogId, out localeId);
            expands.Remove(ZnodeConstant.Pricing);

            sorts.Add("DisplayOrder", "-1");
            KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);

            if (searchResult.Products != null)
            {
                DSAttributeSwatchHelper attributeSwatchHelper = new DSAttributeSwatchHelper();
                attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "drewcolor");
            }

            #region Commented Code swatch code
            //ImageHelper image = new ImageHelper(portalId);
            //string ProductImageName = "";

            //string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);

            //string _imagePath = "";
            //string _swatchImagePath = "";


            //int index = 0;

            ////ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //index = ImageSmallPath.LastIndexOf('/');
            //if (index != -1)
            //    _imagePath = ImageSmallPath.Substring(0, index) + "/";

            ////OriginalImagepath = http://localhost:44762/Data/Media/
            //index = OriginalImagepath.LastIndexOf('/');
            //if (index != -1)
            //{
            //    _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //    _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //}
            //if (searchResult.Products != null)
            //{
            //    List<WebStoreGroupProductModel> simage = GetSwatchImages(String.Join(",", searchResult.Products.Select(x => x.ZnodeProductId).ToList()), _imagePath, _swatchImagePath, "PLP");
            //    foreach (SearchProductModel prd in searchResult.Products)
            //    {

            //        if (prd.AssociatedGroupProducts == null)
            //            prd.AssociatedGroupProducts = new List<WebStoreGroupProductModel>();
            //        prd.AssociatedGroupProducts = simage.Where(x => x.PublishProductId == prd.ZnodeProductId).Select
            //            (t => new WebStoreGroupProductModel()
            //            {
            //                ImageMediumPath = image.GetImageHttpPathSmall(t.ImageMediumPath),
            //                ImageThumbNailPath = t.ImageThumbNailPath
            //            }).ToList<WebStoreGroupProductModel>();
            //    }
            //}
            #endregion

            return searchResult;

        }
        private static void GetParametersValueForFilters(FilterCollection filters, out int portalId, out int catalogId, out int localeId)
        {
            int.TryParse(filters.Where(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out portalId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out catalogId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out localeId);
        }
    }
}
