﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Extensions;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache.Cache
{
    public class DSCustomCache : BaseCache, IDSCustomCache
    {
        #region Private Variables
        private readonly IDSCustomService _dsCustomService;
        #endregion

        #region Constructor
        public DSCustomCache(IDSCustomService dsCustomService)
        {
            _dsCustomService = dsCustomService;
        }
        #endregion

        //Get list of Publish Category.
        public virtual string GetInventoyGrid(int PublishProductId, string color, string routeUri, string routeTemplate)
        {
            //Get data from cache.
            //string data ="";

            string data = GetFromCache(routeUri);
            //if (IsNull(data))
            //{
            //Get Publish Category list.
            DSCustomListModel list = _dsCustomService.GetInventoyGrid(PublishProductId, color);
            if (list.InventoryList.Count > 0)
            {
                //Create response.
                DSCustomResponse response = new DSCustomResponse { InventoryList = list.InventoryList };
                //apply pagination parameters.
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri, routeTemplate, response);
            }
            //}
            return data;
        }

    }
}
