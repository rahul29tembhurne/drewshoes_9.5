﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class SamplePortalListModel : BaseListModel
    {
        public List<SamplePortalModel> PortalList { get; set; }
    }
}
