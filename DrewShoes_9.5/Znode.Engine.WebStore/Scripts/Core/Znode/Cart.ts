﻿declare var coupons: any;
class Cart extends ZnodeBase {
    constructor() {
        super();
    }

    Init() {
        Cart.prototype.RestrictEnterButton();
    }

    RestrictEnterButton(): any {
        $('.frmCartQuantity').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    }

    UpdateCartQauntity(control, isSubmitChekout = false): boolean {
        var productId: number = parseInt($(control).attr("data-cart-productId"));
        var minQuantity: number = parseInt($(control).attr("data-cart-minquantity"));
        var maxQuantity: number = parseInt($(control).attr("data-cart-maxquantity"));
        var errorMessageField = $(control).parent().find("#quantity_error_msg_" + productId);
        var errorQuantityMessageField = $(control).parent().find("#avl_quantity_error_msg_" + productId);
        errorMessageField.text('');
        errorQuantityMessageField.text('');
        var inventoryRoundOff: number = parseInt($(control).attr("data-inventoryRoundOff"));
        var selectedQty: string = $(control).val();
        var decimalPoint: number = 0;
        var decimalValue: number = 0;
        let result: boolean = false;

        if (selectedQty.split(".")[1] != null) {
            decimalPoint = selectedQty.split(".")[1].length;
            decimalValue = parseInt(selectedQty.split(".")[1]);
        }
        if (this.CheckDecimalValue(decimalPoint, decimalValue, inventoryRoundOff, productId, errorMessageField)) {
            if (this.CheckIsNumeric(selectedQty, productId, errorMessageField)) {
                if (this.CheckMinMaxQuantity(parseInt(selectedQty), minQuantity, maxQuantity, productId, errorMessageField)) {
                    $("#btnCompleteCheckout").removeClass('disable-anchor');
                    $("#paypal-express-checkout").removeClass('disable-anchor');
                    $("#checkOut-link").removeAttr('href');
                    result = true;

                    if (isSubmitChekout === undefined || isSubmitChekout === false)
                        $(control).closest("form").submit();
                }
            }
        }
        return result;
    }

    CheckDecimalValue(decimalPoint: number, decimalValue: number, inventoryRoundOff: number, productId: number, errorMessageField: any): boolean {
        if (isNaN(decimalValue) && decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            errorMessageField.text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            $("#checkOut-link").addClass('disable-anchor');
            return false;
        }
        if (isNaN(decimalValue)) {
            errorMessageField.text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            $("#checkOut-link").addClass('disable-anchor');
            return false;
        }
        return true;
    }

    CheckIsNumeric(selectedQty: string, productId: number, errorMessageField: any): boolean {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            errorMessageField.text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            $("#checkOut-link").addClass('disable-anchor');
            return false;
        }
        return true;
    }
    CheckMinMaxQuantity(selectedQty: number, minQuantity: number, maxQuantity: number, productId: number, errorMessageField: any): boolean {
        if (selectedQty < minQuantity || selectedQty > maxQuantity) {
            errorMessageField.text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + ZnodeBase.prototype.getResourceByKeyName("To") + maxQuantity + ZnodeBase.prototype.getResourceByKeyName("FullStop"));
            $("#checkOut-link").addClass('disable-anchor');
            return false;
        }
        return true;
    }

    GetShippingId(control): void {
        Checkout.prototype.ShowLoader();
        var shippingId = $(control).data('shippingid');

        if (typeof shippingId == undefined || shippingId == null || shippingId == "")
            shippingId = 0;

        var zipCode = $("#zipcode").val();
        $("#checkOut-link").attr('href', '/Checkout/Index?ShippingId=' + shippingId);
        Endpoint.prototype.GetCart(shippingId, zipCode, function (response) {
            $(".order-summary").html(response);
            var orderTotal = $("[data-test-selector='hdgOrderGrandTotal']").html();
            (orderTotal != undefined && orderTotal!="")? $("#dynamic-cart-order-total").html(orderTotal) : "";
            Checkout.prototype.HideLoader();
        })
    }

    GetShippingEstimates(): any {
        var zipCode = $("#zipcode").val();
        var zipCodeRegexp = new RegExp("^[- +()]*[0-9][- +()0-9]*$");
        if (zipCode == undefined || zipCode == "") {
            $("#zipcodeerrormessage").text(ZnodeBase.prototype.getResourceByKeyName("ZipCodeError"));
        }
        else if (!zipCodeRegexp.test(zipCode)) {
            $("#zipcodeerrormessage").text(ZnodeBase.prototype.getResourceByKeyName("NumericZipCodeError"));
        }
        else {
            $("#zipcodeerrormessage").text("");
            Cart.prototype.ShowHideCancelButton();
            $("#shippingOptionsContainer").html(ZnodeBase.prototype.getResourceByKeyName("ZipCodeMessage"));
            Endpoint.prototype.GetShippingEstimates(zipCode, function (res) {
                if (res != null && res != "") {
                    var dynamicHtml = "";
                    if (res.shippingOptions != "" && res.shippingOptions != null) {
                        dynamicHtml = "<div class='col-xs-12 nopadding'>";
                        for (var arrayCounter = 0; arrayCounter < res.shippingOptions.length; ++arrayCounter) {
                            var shippingCost = res.shippingOptions[arrayCounter].FormattedShippingRate;
                            var shippingDesc = res.shippingOptions[arrayCounter].Description;
                            var shippingCode = res.shippingOptions[arrayCounter].ShippingCode;
                            var approximateArrival = res.shippingOptions[arrayCounter].ApproximateArrival;
                            var isSelected = res.shippingOptions[arrayCounter].IsSelected;
                            var shippingId = res.shippingOptions[arrayCounter].ShippingId;
                            //commented this line to hide due date as per requirement , Uncomment this line to show Due date on shipping
                            //if (approximateArrival != undefined && approximateArrival.length > 0) {
                            //    shippingDesc = shippingDesc + " (Due on " + approximateArrival + ")";
                            //}
                            if (shippingCode.toLowerCase() != "FreeShipping".toLowerCase()) {
                                if (isSelected) {
                                    dynamicHtml += "<div class='form-group'><div class='col-xs-6 nopadding'><div class='styled-input mr-0'><input type='radio' checked='" + isSelected + "' onclick='Cart.prototype.GetShippingId(this)' name='shippingOptions' data-shippingId='" + shippingId + "' data-shippingCode='" + shippingCode + "' id='" + shippingId + "'/><label for='" + shippingId + "'>" + shippingDesc + "</label></div></div><div class='col-xs-6 nopadding'>" + shippingCost + "</div></div>";
                                }
                                else {
                                    dynamicHtml += "<div class='form-group'><div class='col-xs-6 nopadding'><div class='styled-input'><input type='radio' onclick='Cart.prototype.GetShippingId(this)' name='shippingOptions' data-shippingId='" + shippingId + "' data-shippingCode='" + shippingCode + "' id='" + shippingId + "'/><label for='" + shippingId + "'>" + shippingDesc + "</label></div></div><div class='col-xs-4 nopadding'>" + shippingCost + "</div></div>";
                                }
                            }
                        }
                        dynamicHtml += "</div>";
                        $("#shippingOptionsContainer").html("");
                        $("#shippingOptionsContainer").html(dynamicHtml);
                        if (res.shippingOptions.filter(function (a) { return a.IsSelected == true }).length > 0) {
                            Cart.prototype.GetShippingId($("#shippingOptionsContainer input[type='radio']:checked"));
                        }
                    } else {
                        $("#shippingOptionsContainer").html(ZnodeBase.prototype.getResourceByKeyName("NoShippingOptionsFound"));
                    }
                }
            });
        }
    }



    ShowHideCancelButton(): void {
        if ($("#zipcode").val().length > 0) {
            $("#CancleShippingEstimator").show();
        } else {
            $("#CancleShippingEstimator").hide();
        }
    }
    ClearShippingEstimates(): void {
        $("#zipcode").val('');

            $("#shippingOptionsContainer input[type='radio']:checked").prop('checked', false)

        Cart.prototype.GetShippingId($("#shippingOptionsContainer input[type='radio']:checked"));

        $("#shippingOptionsContainer").html('');
        $("#CancleShippingEstimator").hide();
    }

    ValidateProductQuantity(): void {
        //alert('hiii');
        //Nivi Code
        let finalResult: boolean = false;
        let productId: number;

        $("div.cart-products table tbody tr").each(function () {
            let qtyTextbox = $(this).find('input[name="Quantity"]');

            //Check if Modified Quantity is updated in DB or not
            var originalQty = $(qtyTextbox).attr("data-qty");
            var modifiedQty = $(qtyTextbox).val();

            if (originalQty != modifiedQty.toString()) {
                var sku = $(qtyTextbox).attr("data-cart-sku");
                $("#CartUpdateError").show();
                $("#CartUpdateError").html('<p style="display:inline-block;border: 1px solid red;padding: 5px;width:100%;" class="text-danger">Please Click UPDATE for item<b>     ' + sku + ' </b>before proceeding to checkout</p>');
                $("#checkOut-link").each(function () {
                    $("#checkOut-link").addClass('disable-anchor');
                    $("#checkOut-link").removeAttr('href');
                });
                return finalResult = false;
            }
            else {
                $("#CartUpdateError").html('');
                $("#CartUpdateError").hide();
            }
            productId = parseInt($(qtyTextbox).attr("data-cart-productId"));
            finalResult = Cart.prototype.UpdateCartQauntity(qtyTextbox, true);
            if (finalResult === false) {
                $("#quantity_error_msg_" + productId).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + $(qtyTextbox).attr("data-cart-minquantity") + ZnodeBase.prototype.getResourceByKeyName("To") + $(qtyTextbox).attr("data-cart-maxquantity") + ZnodeBase.prototype.getResourceByKeyName("FullStop"));
                $("#checkOut-link").addClass('disable-anchor');
                $("#checkOut-link").removeAttr('href');
                return finalResult;
            }
        });

        if (finalResult) {
            location.href = window.location.protocol + "//" + window.location.host + "/checkout/index";
        }
    }
    //    let finalResult: boolean = false;
    //    let productId: number;
        
    //    $("div.cart-products table tbody tr").each(function () {
    //        let qtyTextbox = $(this).find('input[name="Quantity"]');

    //        productId = parseInt($(qtyTextbox).attr("data-cart-productId"));
    //        finalResult = Cart.prototype.UpdateCartQauntity(qtyTextbox, true);

    //        if (finalResult === false) {
    //            $("#quantity_error_msg_" + productId).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + $(qtyTextbox).attr("data-cart-minquantity") + ZnodeBase.prototype.getResourceByKeyName("To") + $(qtyTextbox).attr("data-cart-maxquantity") + ZnodeBase.prototype.getResourceByKeyName("FullStop"));
    //            $("#checkOut-link").addClass('disable-anchor');
    //            $("#checkOut-link").removeAttr('href');
    //            return finalResult;
    //        }
    //    });

    //    if (finalResult) {
    //        location.href = window.location.protocol + "//" + window.location.host + "/checkout/index";
    //    }
    //}

    EnableDisableCheckoutButton(): void {
        if ($('#hdnInsufficientQuantity').val() == 'True') {
            $("#checkOut-link").addClass('disable-anchor');
            $("#btnCompleteCheckout").addClass('disable-anchor');
            $("#paypal-express-checkout").addClass('disable-anchor');
            $("#checkOut-link").removeAttr('href');
        }
        ZnodeBase.prototype.HideLoader();
    }
    EnableDisableMenuIcon(): void {        
        if ($("#others-menu .dropdown").length === 0) {            
            $(".nav-drop-icon").hide();
        }
        else {            
            $(".nav-drop-icon").show();
        }

        $(".control_next").on("click", function () {
            $("#others-menu").toggle();
        });

        ZnodeBase.prototype.HideLoader();
    }

    ShowCouponsMessages(Coupons:any): void {
        var htmlString = "";
         coupons = Coupons;
        if (coupons.length > 0) {
        for (var dataIndex = 0; dataIndex < coupons.length; dataIndex++) {
            var style = coupons[dataIndex].CouponApplied ? "success-msg padding-top" : "error-msg";
            var message = coupons[dataIndex].PromotionMessage;
            var couponCode = coupons[dataIndex].Code;
            Checkout.prototype.RemoveDiscountMessages();
            htmlString = htmlString + "<p class='text-break " + style + "'>" + "<a class='zf-close' onclick='Cart.prototype.RemoveAppliedCoupon(" + dataIndex + ")' style='cursor:pointer;color:#cc0000;padding-right:3px;' title='Remove Coupon Code'></a>" + "<b>" + couponCode + "</b>" + " - " + message + "</p>";
        }
        htmlString = htmlString + "</div>";
        $("#couponMessageContainer").html("");
            $("#couponMessageContainer").html(htmlString);
        }
    }

    RemoveAppliedCoupon(couponIndex): any {
        var _code = coupons[couponIndex].Code;
        coupons = new Array();
        Checkout.prototype.RemoveCoupon(_code);
    }

}

$(document).on("keypress", "#cart-quantity", function (e) {
    var key = e.keyCode || e.which;
    if ((47 < key) && (key < 58) || key === 8) {
        return true;
    }
    return false;
});

$(document).on("cut copy paste", "#cart-quantity", function (e) {
    e.preventDefault();
});
